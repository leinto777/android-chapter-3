package com.example.myapplication

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

data class FoodClass(
    val image: Int,
    val name: String,
    val price: String,
    val dataDescription: String?
)
